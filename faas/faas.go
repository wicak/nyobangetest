package faas

import (
	"net/http"

	"gitlab.com/wicak/nyobangetest/handlers/rest"
)

func Translate(w http.ResponseWriter, r *http.Request) {
	rest.TranslateHandler(w, r)
}
