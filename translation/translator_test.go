package translation_test

import (
	"testing"

	"gitlab.com/wicak/nyobangetest/translation"
)

func TestTranslate(t *testing.T) {

	res := translation.Translate("hello", "english")
	if res != "hello" {
		t.Errorf(`expected "hello" but received %s\n`, res)
	}

	res = translation.Translate("hello", "german")
	if res != "hallo" {
		t.Errorf(`expected "hello" but received %s\n`, res)
	}

	res = translation.Translate("hello", "finnish")
	if res != "hei" {
		t.Errorf(`expected "hello" but received %s\n`, res)
	}

	res = translation.Translate("hello", "dutch")
	if res != "" {
		t.Errorf(`expected "hello" but received %s\n`, res)
	}

}
