package rest

import (
	"encoding/json"
	"net/http"
	"strings"

	"gitlab.com/wicak/nyobangetest/translation"
)

type Resp struct {
	Language    string `json:"language"`
	Translation string `json:"translation"`
}

// type TranslateHandler struct {
// 	service Translator
// }

// type Translator interface {
// 	Translate(word string, language string) string
// }

// func NewTranslateHandler(service Translator) *TranslateHandler {
// 	return &TranslateHandler{
// 		service: service,
// 	}
// }

const defaultLanguage = "english"

func /*(t *TranslateHandler) */ TranslateHandler(w http.ResponseWriter, r *http.Request) {
	enc := json.NewEncoder(w)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	language := r.URL.Query().Get("language")
	if language == "" {
		language = defaultLanguage
	}

	word := strings.ReplaceAll(r.URL.Path, "/", "")
	translation := translation.Translate(word, language)
	if translation == "" {
		language = ""
		w.WriteHeader(http.StatusNotFound)
		return
	}
	resp := Resp{
		Language:    language,
		Translation: translation,
	}
	if err := enc.Encode(resp); err != nil {
		panic("unable encode response")
	}
}
