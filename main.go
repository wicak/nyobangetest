package main

import (
	"log"
	"net/http"

	"gitlab.com/wicak/nyobangetest/handlers/rest"
)

func main() {
	addr := ":8088"

	mux := http.NewServeMux()

	mux.HandleFunc("/hello", rest.TranslateHandler)

	log.Printf("listening on %s\n", addr)
	log.Fatal(http.ListenAndServe(addr, mux))
}

type Resp struct {
	Language     string `json:"language"`
	Transalation string `json:"translation"`
}
