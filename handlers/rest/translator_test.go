package rest_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/wicak/nyobangetest/handlers/rest"
)

type stubbedService struct{}

func (s *stubbedService) Translate(word string, language string) string {
	if word == "foo" {
		return "bar"
	}
	return ""
}

func TestTranslateAPI(t *testing.T) {

	tt := []struct {
		Name                string
		Endpoint            string
		StatusCode          int
		ExpectedLanguage    string
		ExpectedTranslation string
	}{
		{
			Name:                "english language",
			Endpoint:            "/hello",
			StatusCode:          http.StatusOK,
			ExpectedLanguage:    "english",
			ExpectedTranslation: "hello",
		},
		{
			Name:                "german language",
			Endpoint:            "/hello?language=german",
			StatusCode:          http.StatusOK,
			ExpectedLanguage:    "german",
			ExpectedTranslation: "hallo",
		},
		{
			Name:                "dutch language",
			Endpoint:            "/hello?language=dutch",
			StatusCode:          http.StatusNotFound,
			ExpectedLanguage:    "",
			ExpectedTranslation: "",
		},
	}

	// h := rest.NewTranslateHandler(&stubbedService{})
	// handler := http.HandlerFunc(h.TranslateHandler)

	handler := http.HandlerFunc(rest.TranslateHandler)

	for _, test := range tt {

		t.Run(test.Name, func(t *testing.T) {
			rr := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", test.Endpoint, nil)

			handler.ServeHTTP(rr, req)

			if rr.Code != test.StatusCode {
				t.Errorf(`expected status 200 but received %d`, rr.Code)
			}

			var resp rest.Resp
			json.Unmarshal(rr.Body.Bytes(), &resp)

			if resp.Language != test.ExpectedLanguage {
				t.Errorf(`expected language "%s" but received %s`, test.ExpectedLanguage, resp.Language)
			}

			if resp.Translation != test.ExpectedTranslation {
				t.Errorf(`expected translation "%s" but received %s`, test.ExpectedTranslation, resp.Translation)
			}
		})

	}

}
